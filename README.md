### What is this repository? ###
Allow to jump between class header and implementation files.
It change directory *dir/**Source**/class.**cpp** <-> dir/**Include**/class.**hpp***

### Set up ###
To switch file call *CPlaneJumpHeaderSource()* or

add to .vimrc *map <shortcut> :call CPlaneJumpHeaderSource()<CR>*