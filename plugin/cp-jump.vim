" jump between hpp <-> cpp file
function! CPlaneJumpHeaderSource()
    let s:flipname = ""
    if( match(expand("%"),'\.cpp') > 0 )
        let s:flipname = substitute(expand("%:p"),'\.cpp\(.*\)','\.hpp\1',"")
        let s:flipname = substitute(s:flipname,'Source','Include',"")
    endif
    if( match(expand("%"),'\.hpp') > 0 )
        let s:flipname = substitute(expand("%:p"),'\.hpp\(.*\)','\.cpp\1',"")
        let s:flipname = substitute(s:flipname,'Include','Source',"")
    endif
    if( strlen(s:flipname) == 0)
        echo "Incorrect file format"
        return
    endif
    if filereadable(s:flipname)
        exe ":e " . s:flipname
    else
        echo "Can not open file: " . s:flipname
        return
    endif
endfun
